<?php

use dektrium\user\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\web\View;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch $searchModel
 */

$this->title = 'Рекламный блок';
$this->params['breadcrumbs'][] = $this->title;

//$this->beginBlock('content-header');
echo $this->title;
//$this->endBlock();
?>

<? //= $this->render('/admin/_menu') ?>

<div class="box box-success">

    <div class="box-body">
        <table class="table table-striped table-bordered">
            <tr>

                <th>Id</th>
                <th>Изображение</th>
                <th>Ссылка</th>
                <th>Действие</th>
            </tr>
            <tbody>
            <?php foreach ($advers as $adver) : ?>
                <tr>
                    <td><?= $adver['id'] ?></td>
                    <td>
                        <img class="preview" src="http://imagecorp.ru<?= Yii::getAlias('@advertising') ?>/<?= $adver['image'] ?>">
                        <input accept="image/*;capture=camera" class="upload-img-input"
                               value="<?= Yii::$app->request->getCsrfToken() ?>" type="file" style="display: none">
                        <a href="#" title="Редактировать" aria-label="Редактировать" class="upload-img" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                    <td><input type="text" class="adver-text" value="<?= $adver['link'] ?>"></td>
                    <td><a class="edit-adver" data-id="<?= $adver['id'] ?>" style="cursor:pointer;">Сохранить</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->registerJsFile('js/myscript.js', ['depends' => 'frontend\assets\AppAsset']); ?>