<?php

$this->title = 'Заявки на оплату';
$this->params['breadcrumbs'][] = $this->title;
echo $this->title;
?>


<div class="box">

    <div class="box-body">
        <table class="table table-striped table-bordered">
            <tr>
                <th>ID</th>
                <th>Логин пользователя</th>
                <th>Ф.И. пользователя</th>
                <th>Контактный телефон</th>
                <th>Реквизиты к оплате</th>
                <th>Email</th>
                <th>Общая сумма</th>
                <th>Сумма пользователя</th>
                <th>Действие</th>
            <tbody>
            <?php foreach ($income_partners as $income_partner) : ?>
                <tr>
                    <td><?= $income_partner['requisites']['id'] ?></td>
                    <td><?= $income_partner['user']['username'] ?></td>
                    <td><?= $income_partner['requisites']['last_name'] . ' ' . $income_partner['requisites']['first_name'] ?></td>
                    <td><?= $income_partner['requisites']['phone_number'] ?></td>
                    <td><?= $income_partner['requisites']['requisites'] ?></td>
                    <td><?= $income_partner['user']['email'] ?></td>
                    <td><?= $income_partner['sum'] ?></td>
                    <td><?= $income_partner['sum'] * 0.15 ?></td>
                    <td><a class="confirm-application" data-id="<?= $income_partner['id_partner'] ?>" data-requisites="<?= $income_partner['requisites']['id'] ?>" style="cursor:pointer;">Подтвердить</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
</div>

<?php $this->registerJsFile('js/myscript.js', ['depends' => 'frontend\assets\AppAsset']); ?>