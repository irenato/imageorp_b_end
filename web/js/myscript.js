$(document).ready(function () {
    if ($('#question-question_type_id').val() !== 3) {
        $('#question-answers_cnt option').each(function (e) {
            if ($(this).val() == 1) {
                $(this).css('display', 'none');
            }
            $(this).removeAttr('disabled');
        });
    }
    $('#question-question_type_id').click(function () {
        //console.log('type ' + $('#question-question_type_id').val());
        if ($('#question-question_type_id').val() == 3) {
            $('#question-answers_cnt').val(1);
            $('#question-answers_cnt option').each(function (e) {
                if ($(this).val() != 1) {
                    $(this).attr('disabled', 'disabled');
                }
                else {
                    $(this).removeAttr('disabled', 'disabled');
                }
            });

        } else if ($('#question-question_type_id').val() == 5 || $('#question-question_type_id').val() == 8 || $('#question-question_type_id').val() == 9) {
            $('#question-answers_cnt').val(2);
            $('#question-answers_cnt option').each(function (e) {
                if ($(this).val() != 2) {
                    $(this).attr('disabled', 'disabled');
                }
                else {
                    $(this).removeAttr('disabled', 'disabled');
                }
            });
        }
        else {
            $('#question-answers_cnt').val(2);
            $('#question-answers_cnt option').each(function (e) {
                if ($(this).val() == 1) {
                    $(this).css('display', 'none');
                }
                $(this).removeAttr('disabled');
            });
        }
    });

});

$('body').on('click', 'a.confirm-application', function (e) {
    e.preventDefault();
    var checked_el = $(this),
        request = $.ajax({
            data: {
                'id_partner': checked_el.attr('data-id'),
                'id_requisites': checked_el.attr('data-requisites'),
            },
            url: '/user/admin/confirm-application',
            type: 'post',
            dataType: 'html',
        });
    request.done(function (result) {
        if (result == 'Done!')
            checked_el.closest('tr').detach();
    })
//    detach();
})

var edit_button;

$('body').on('click', 'a.upload-img', function (e) {
    edit_button = $(this);
    e.preventDefault();
    $(this).closest('td').find('input.upload-img-input').trigger('click');
})

var upload = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    var request = $.ajax({
        data: formData,
        url: '/user/admin/upload-image',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false,
    });
    request.done(function (url) {
        edit_button.closest('td').find('img').attr('src', url);
    });
}

$('body').on('change', 'input.upload-img-input', function (e) {
    e.preventDefault();
    var file = this.files;
    if (file.length == 1)
        upload(file);
})

$('body').on('click', 'a.edit-adver', function (e) {
    edit_button = $(this);
    e.preventDefault();
    var id = $(this).attr('data-id'),
        img = $(this).closest('tr').find('img').attr('src'),
        link = $(this).closest('tr').find('input.adver-text').val(),
        request = $.ajax({
            data: {
                'id': id,
                'img': img,
                'link': link,
            },
            url: '/user/admin/edit-current-adver',
            type: 'post',
            dataType: 'html',
        });
    request.done(function (result) {
        if (result == 'Done!')
            location.reload();
    })
})

//-------------------------------------------
// confirm comments
//-------------------------------------------
$('body').on('click', "a.btn-for-action", function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id'),
        do_it_now = $(this).attr('data-action'),
        request =  $.ajax({
            data: {
                'id': id,
            },
            url: '/user/admin/' + do_it_now + '-comment',
            type: 'post',
            dataType: 'html',
        });
    request.done(function (result) {
        if (result == 'Done!')
            location.reload();
    })
})

