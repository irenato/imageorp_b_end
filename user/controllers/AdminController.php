<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace backend\user\controllers;

use Yii;
use frontend\models\UserComments;
use frontend\models\ReferIncome;
use frontend\models\AdvertisingBlock;
use yii\data\Pagination;
use dektrium\user\models\User;
use frontend\models\PaymentRequisites;
use dektrium\user\controllers\AdminController as BaseController;


class AdminController extends BaseController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionConfirmComment()
    {
        $request = Yii::$app->request;
        $comment = UserComments::findOne(['id' => $request->post('id')]);
        $comment->status = 1;
        if ($comment->update())
            return 'Done!';
    }

    public function actionCancelComment()
    {
        $request = Yii::$app->request;
        $comment = UserComments::findOne(['id' => $request->post('id')]);
        $comment->status = 0;
        if ($comment->update())
            return 'Done!';
    }

    public function actionDeleteComment()
    {
        $request = Yii::$app->request;
        $comment = UserComments::findOne(['id' => $request->post('id')]);
        if ($comment->delete())
            return 'Done!';
    }

    public function actionComments()
    {
        $pagination = new Pagination(['defaultPageSize' => 20,
            'totalCount' => UserComments::find()->count()]);
        $comments = UserComments::find()
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->orderBy('id DESC')
            ->all();
        return $this->render('comments', [
            'comments' => $comments,
            'pagination' => $pagination,
        ]);
    }

    public function actionEditAdvers()
    {
        Yii::$app->view->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.16/css/bulma.min.css');
        Yii::$app->view->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js', ['depends' => 'yii\web\YiiAsset']);
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
        Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/flexie/1.0.3/flexie.min.js', ['depends' => 'yii\web\YiiAsset']);
        $advers = AdvertisingBlock::selectAdvers();

        return $this->render('edit-advers', [
            'advers' => $advers,
        ]);
    }

    public function actionApplication()
    {
        $temp_ids_partners = ReferIncome::find()
            ->select(['id_partner'])
            ->where(['status' => 1])
            ->groupBy(['id_partner'])
            ->asArray()
            ->all();
        $income_partners = array();
        if ($temp_ids_partners) {
            $ids_partners = array_column($temp_ids_partners, 'id_partner');
            $i = -1;
            foreach ($ids_partners as $id_partner) {
                $i++;
                $income_partners[$i]['user'] = User::find()
                    ->select(['username', 'email'])
                    ->where(['id' => $id_partner])
                    ->asArray()
                    ->one();
                $income_partners[$i]['requisites'] = PaymentRequisites::find()
                    ->select(['id', 'first_name', 'last_name', 'phone_number', 'requisites'])
                    ->where(['id_user' => $id_partner])
                    ->andWhere(['status' => 0])
                    ->asArray()
                    ->one();

                $income_partners[$i]['sum'] = array_sum(array_column(ReferIncome::find()
                    ->select(['income'])
                    ->where(['id_partner' => $id_partner])
                    ->andWhere(['status' => 1])
                    ->asArray()
                    ->all(), 'income'));
                $income_partners[$i]['id_partner'] = $id_partner;
            }
        }
        return $this->render('application', [
            'income_partners' => $income_partners
        ]);

    }

    public function actionApplicationsConfirmed()
    {
        $temp_ids_partners = ReferIncome::find()
            ->select(['id_partner'])
            ->where(['status' => 2])
            ->groupBy(['id_partner'])
            ->asArray()
            ->all();
        $income_partners = array();
        if ($temp_ids_partners) {
            $ids_partners = array_column($temp_ids_partners, 'id_partner');
            $i = -1;
            foreach ($ids_partners as $id_partner) {
                $i++;
                $income_partners[$i]['user'] = User::find()
                    ->select(['username', 'email'])
                    ->where(['id' => $id_partner])
                    ->asArray()
                    ->one();
                $income_partners[$i]['sum'] = array_sum(array_column(ReferIncome::find()
                    ->select(['income'])
                    ->where(['id_partner' => $id_partner])
                    ->andWhere(['status' => 2])
                    ->asArray()
                    ->all(), 'income'));
                $income_partners[$i]['requisites'] = PaymentRequisites::find()
                    ->select(['id', 'first_name', 'last_name', 'phone_number', 'requisites'])
                    ->where(['id_user' => $id_partner])
                    ->andWhere(['status' => 1])
                    ->asArray()
                    ->one();
                $income_partners[$i]['id_partner'] = $id_partner;
            }
        }
        return $this->render('applications-confirmed', [
            'income_partners' => $income_partners
        ]);
    }

    public function actionConfirmApplication()
    {
        $request = Yii::$app->request;
        $requisites = PaymentRequisites::findOne(['id' => $request->post('id_requisites'), 'status' => 0]);
        $requisites->status = 1;
        if (ReferIncome::updateAll(['status' => 2], ['and', ['id_partner' => $request->post('id_partner')], ['status' => 1]]) && $requisites->update())
            return 'Done!';
    }

    public function actionUploadImage()
    {
        if (isset($_FILES['file'])) {
            $file = $_FILES['file'];
            $temp = explode("/", $file['type']);
            if ($temp[1] == 'png' || $temp[1] == 'jpeg' || $temp[1] == 'jpg' || $temp[1] == 'bmp') {
                $ext = '.' . $temp[1];
                $name = time();
                if (move_uploaded_file($file['tmp_name'], '/home/i/imagec1s/imagecorp.ru/frontend/web/uploads/advertising/' . $name . $ext))
                    echo 'http://imagecorp.ru' . Yii::getAlias('@advertising') . '/' . $name . $ext;
            }
        }
    }

    public function actionEditCurrentAdver()
    {
        $request = Yii::$app->request;
        if (AdvertisingBlock::editAdver($request->post('id'), $request->post('img'), $request->post('link')))
            echo "Done!";
    }
}
