<?php
use dektrium\user\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\web\View;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch $searchModel
 */

$this->title = 'Комментарии пользователей';
$this->params['breadcrumbs'][] = $this->title;

//$this->beginBlock('content-header');
echo $this->title;
//$this->endBlock();
?>

<? //= $this->render('/admin/_menu') ?>

    <div class="box box-success">

        <div class="box-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Id комментария</th>
                    <th>Имя пользователя</th>
                    <th>Email</th>
                    <th>Регистрационный IP</th>
                    <th>Комментарий</th>
                    <th>Дата</th>
                    <th>Действие</th>
                </tr>
                <tbody>
                <?php foreach ($comments as $comment) : ?>
                    <tr>
                        <td><?= $comment->id ?></td>
                        <td><?= $comment->user->username ?></td>
                        <td><?= $comment->user->email ?></td>
                        <td><?= $comment->user->registration_ip ?></td>
                        <td><?= $comment->comment ?></td>
                        <td><?= date('Y-m-d H:i:s', $comment->created_at) ?></td>
                        <td>
                            <?php if ($comment->status == 0) : ?>
                                <a class="btn-for-action btn btn-xs btn-success btn-block"
                                   href="#" data-action="confirm" data-id="<?= $comment->id ?>">Подтвердить</a>
                            <?php else : ?>
                                <a class="btn-for-action btn btn-xs btn-warning btn-block"
                                   href="#" data-action="cancel" data-id="<?= $comment->id ?>">Убрать из просмотра</a>
                            <?php endif; ?>
                            <a class="btn-for-action btn btn-xs btn-danger btn-block"
                               href="#" data-action="delete" data-id="<?= $comment->id ?>">Удалить</a>
                        </td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?= LinkPager::widget([
                'pagination' => $pagination,]) ?>
        </div>
    </div>

<?php $this->registerJsFile('js/myscript.js', ['depends' => 'frontend\assets\AppAsset']); ?>