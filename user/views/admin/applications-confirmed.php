<?php

$this->title = 'Завершенные заявки';
$this->params['breadcrumbs'][] = $this->title;

//$this->beginBlock('content-header');
echo $this->title;
//$this->endBlock();
?>

<? //= $this->render('/admin/_menu') ?>

<div class="box">

    <div class="box-body">
        <table class="table table-striped table-bordered">
            <tr>
                <th>ID</th>
                <th>Логин пользователя</th>
                <th>Ф.И. пользователя</th>
                <th>Контактный телефон</th>
                <th>Реквизиты к оплате</th>
                <th>Email</th>
                <th>Общая сумма</th>
                <th>Сумма пользователя</th>
                <tbody>
                <?php foreach ($income_partners as $income_partner) : ?>
                    <tr>
                        <td><?= $income_partner['requisites']['id'] ?></td>
                        <td><?= $income_partner['user']['username'] ?></td>
                        <td><?= $income_partner['requisites']['last_name'] . ' ' . $income_partner['requisites']['first_name'] ?></td>
                        <td><?= $income_partner['requisites']['phone_number'] ?></td>
                        <td><?= $income_partner['requisites']['requisites'] ?></td>
                        <td><?= $income_partner['user']['email'] ?></td>
                        <td><?= $income_partner['sum'] ?></td>
                        <td><?= $income_partner['sum'] * 0.15 ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
        </table>
    </div>

